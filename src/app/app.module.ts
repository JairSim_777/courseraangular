import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

//routing
import { Routes, RouterModule } from '@angular/router';
// import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { TravelDestinationComponent } from './components/travel/travel-destination/travel-destination.component';
import { DestinationListComponent } from './components/travel/destination-list/destination-list.component';
import { TargetComponent } from './components/target/target.component';
import { DestinationDetailComponent } from './components/destination-detail/destination-detail.component';
import { TravelDestinationFormComponent } from './components/travel-destination-form/travel-destination-form.component';

import { DestinationApiClient } from './models/destinationApiClient.models';

//Redux
import { StoreModule as NgRxStoreModule, ActionReducerMap } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import {
  StatetTravelDestination,
  DestinosViajesEffects,
  reducerDestinosViajes,
  intializeDestinosViajesState} from './models/stateTravelDestination.models';

  // Configuracion Redux Devtools
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full' },
  {path: 'home', component: DestinationListComponent },
  {path: 'destino', component: DestinationDetailComponent },
  {path: "**", redirectTo: "home"}
];


// redux  init------------------------
// redux init - variable locales para el ngModule
// Definiendo el estado global de la aplicacion
export interface AppState {
    //Modulos
    destinos: StatetTravelDestination;
  }

  //Reducers globales de la aplicacion
  const reducers: ActionReducerMap<AppState> = {
    destinos: reducerDestinosViajes
  };

  // inicializacion
  let reducersInitialState = {
    destinos: intializeDestinosViajesState()
  };
  // redux fin ----------------------


@NgModule({
  declarations: [
    AppComponent,
    TravelDestinationComponent,
    DestinationListComponent,
    TargetComponent,
    DestinationDetailComponent,
    TravelDestinationFormComponent
  ],
  imports: [
    BrowserModule,
    // AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes),
    NgRxStoreModule.forRoot(reducers, { initialState: reducersInitialState,
      runtimeChecks:{
        strictStateImmutability: false,
        strictActionImmutability: false,
      }
    }),
    EffectsModule.forRoot([DestinosViajesEffects]),
    StoreDevtoolsModule.instrument()
  ],
  providers: [
    DestinationApiClient
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
