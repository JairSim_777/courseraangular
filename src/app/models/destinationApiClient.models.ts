import { BehaviorSubject, Subject } from 'rxjs';
import { TravelDestination } from './travelDestination.models';

import { Store } from '@ngrx/store';
import { AppState } from '../app.module';
import { Injectable } from '@angular/core';
import { ElegidoFavoritoAction, NuevoDestinoAction } from './stateTravelDestination.models';

@Injectable()
export class DestinationApiClient {
  constructor (private store: Store<AppState>){}

  add(d: TravelDestination){
    this.store.dispatch(new NuevoDestinoAction(d));
  }

  elegir(d: TravelDestination){
    this.store.dispatch(new ElegidoFavoritoAction(d));
  }
}


/*export class DestinationApiClient {
  destinos: TravelDestination[];

  current: Subject <TravelDestination> = new BehaviorSubject<TravelDestination>(null);

  constructor() {
    this.destinos = [];
  }

  add(d: TravelDestination) {
    this.destinos.push(d);
  }

  getAll(): TravelDestination[] {
    return this.destinos;
  }

  getById(id: String): TravelDestination {
    return this.destinos.filter(function(d){return d.id.toString() === id; })[0];
  }

  elegir (d: TravelDestination) {
    this.destinos.forEach(x => x.setSelected(false));
    d.setSelected(true);
    this.current.next(d);
  }

  subcribeOnChange(fn){
    this.current.subscribe(fn);
  }

}*/
