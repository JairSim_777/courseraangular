
import {v4 as uuid} from 'uuid';
// Definicion propiedades de un objeto
export class TravelDestination {
  // flag = selected
  private selected: boolean;
  // que se incluye dentro de la oferta
  public servicios: string[];
  id = uuid();


// name = nm
  // imagen = uu
  constructor(public nm: string, public uu: string, public votes: number = 0){
    this.servicios = ['piscina', 'desayuno', 'bar'];
  }

  isSelected(): boolean {
    return this.selected;
  }

  setSelected(s: boolean) {
    this.selected = s;
  }
  // Manos
  voteUp(): any {
    this.votes++;
  }
  voteDown(): any {
    this.votes--;
  }
}

