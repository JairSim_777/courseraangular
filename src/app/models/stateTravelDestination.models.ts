// Redux (core redux): Gestionar un unico estado global en la aplicacion
// Manejo de estado de la aplicacion
import { Injectable} from '@angular/core';

import { Action } from '@ngrx/store';
import { Actions, Effect, ofType }from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

import { TravelDestination } from './travelDestination.models';

// Estado : Manejo de estado atravez de una interfaz
//Estado global de la aplicacion. storage almacen del estado
export interface StatetTravelDestination {
  //  items destino viajes que nos interesan
  items: TravelDestination[];
  loading: boolean;
  // destino seleccionado favorito
  favorito: TravelDestination;
};

// inicializar el estado
export const intializeDestinosViajesState = function(){
  return {
    items: [],
    loading: false,
    favorito: null
  }
};

//----------------------------------------------------------
// ACCIONES del usuario interactuando con la aplicación,
// que disparan un cambio de estado. interface Action
// Mutar atravez de las acciones

export enum DestinosViajesActionTypes {
  // constantes definidas en un enum, definicion de todos los objetos
  // los string modulos del sistemas
  NUEVO_DESTINO = '[Destino Viajes] Nuevo',
  ELEGIDO_FAVORITO= '[Destino Viajes] Favorito',

 //tipos de eventos manos.  String que identifica el evento
  VOTE_UP = '[Destino Viajes] Vote Up',
  VOTE_DOWN = '[Destino Viajes] Vote Down'
}

 // Accion NuevoDestinoAction
  export class NuevoDestinoAction implements Action {
    // Variable tipo type
    type = DestinosViajesActionTypes.NUEVO_DESTINO;
    constructor(public destino: TravelDestination) {}
  }

  // Accion
  export class ElegidoFavoritoAction implements Action {
    type = DestinosViajesActionTypes.ELEGIDO_FAVORITO;
    constructor(public destino: TravelDestination){}
  }
  // Acciones Manos
  export class VoteUpAction implements Action {
    type = DestinosViajesActionTypes.VOTE_UP;
    constructor (public destino: TravelDestination){}
  }
  // Acciones Manos
  export class VoteDownAction implements Action {
    type = DestinosViajesActionTypes.VOTE_DOWN;
    constructor (public destino: TravelDestination){}
  }

  //-----------------------------------------------------------------
  // rootReducer ??

  // DestinosViajesActions: Union de tipos

  // Agrupar (concatenar) todos los tipos de datos de las acciones  | union de tipos
  export type DestinosViajesActions = NuevoDestinoAction | ElegidoFavoritoAction 
              | VoteUpAction | VoteDownAction;

  //------------------------------------------------------------------------------
  // REDUCERS: cada vez que se dispara una accion. los REDUCERS
  // actuan sobre los state y van mutando el estado

  export function reducerDestinosViajes (
    // estado anterior del sistema
    state: StatetTravelDestination,
    // accion que se esta disparando
    action: DestinosViajesActions
  ): StatetTravelDestination {
    switch (action.type) {
      case DestinosViajesActionTypes.NUEVO_DESTINO: {
        return {
          ...state,
          items: [...state.items, (action as NuevoDestinoAction).destino ]
        };
      }
      case DestinosViajesActionTypes.ELEGIDO_FAVORITO: {
        state.items.forEach(x => x.setSelected(false));
        const fav: TravelDestination = (action as ElegidoFavoritoAction).destino;
        fav.setSelected(true);
        return {
          ...state,
          favorito: fav
        };
      }
      // Manos : Inmutabilidad de estados 
      case DestinosViajesActionTypes.VOTE_UP: {
        const d: TravelDestination = (action as VoteUpAction).destino;
        d.voteUp();
        return{ ...state};
      }
      case DestinosViajesActionTypes.VOTE_DOWN: {
        const d: TravelDestination = (action as VoteDownAction).destino;
        d.voteDown();
        return { ...state};
      }

    }
    return state;
    // Devolver el estado sin mutar
  }

  // EFFECTS: Luego que se dispara una acción (Registrar una accion
  // como consecuencia de otra acción), la accion se pasa a todos los
  // REDUCERS registrados
  // EFFECTS: Dada una accion y un estado del sistema, generan un nuevo estado
  // mutado o sin mutar.
  // EFFECTS: Disparar una funcion en funcion de otra

  @Injectable()
  export class DestinosViajesEffects {
    @Effect()
    nuevoAgregado$: Observable<Action> = this.actions$.pipe(
      // cada vez que se agrega un nuevo destino , se elije como nuevo favorito
      // ofType : filtra el tipo de la accion que interesa
      ofType(DestinosViajesActionTypes.NUEVO_DESTINO),
  // cuando llega la NuevoDestinoAction, disparo el ElegidoFavoritoAction
      map((action: NuevoDestinoAction) => new ElegidoFavoritoAction(action.destino))
    );

    constructor(private actions$: Actions) {}
  }




