import { Component } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
// title = 'courseraAngular6';
// let timeD;

  timeD = new Observable(observer => {
    //Observador de string
    setInterval(() => observer.next(new Date().toString()), 1000);
    return  null;
  });
}
