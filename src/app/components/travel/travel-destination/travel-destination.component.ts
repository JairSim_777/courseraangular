import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { AppState } from 'src/app/app.module';
import { TravelDestination } from '../../../models/travelDestination.models';
import { VoteDownAction, VoteUpAction } from 'src/app/models/stateTravelDestination.models';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-travel-destination',
  templateUrl: './travel-destination.component.html',
  styleUrls: ['./travel-destination.component.css']
})
export class TravelDestinationComponent implements OnInit {
//@Input():  DECORADOR - Argumento . nombre susceptible a ser
//pasado como parametro en el tag travel-destination
// position = numCard
@Input('numC') numCard: number;
@Input() destino: TravelDestination;
// @HostBinding:  Tener una vinculacion directa de un una propiedad (atributo) string
// ('att.class'): Atributo class
@HostBinding('attr.class') cssClass = 'col-md-4';
// Creacion de controlador de evento propio clicked
@Output() clicked: EventEmitter<TravelDestination>;

  constructor(private store: Store<AppState>) {
    this.clicked = new EventEmitter();
  }

  ngOnInit(): void {
  }


// Emitir eventos al componente padre
  ir(){
    this.clicked.emit(this.destino);
    return false;
  }

  //dispatch - Disparar acciones 
  voteUp(){
    this.store.dispatch(new VoteUpAction(this.destino));
    return false;
  }

  voteDown(){
    this.store.dispatch(new VoteDownAction(this.destino));
    return false; 
  }
}
