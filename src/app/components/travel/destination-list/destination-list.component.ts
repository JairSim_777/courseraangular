import { Component, EventEmitter, OnInit, Output } from '@angular/core';

import { TravelDestination } from '../../../models/travelDestination.models';
import { DestinationApiClient} from '../../../models/destinationApiClient.models';

import { AppState } from 'src/app/app.module';

// Redux
import { Store } from '@ngrx/store';
import { ElegidoFavoritoAction, NuevoDestinoAction } from 'src/app/models/stateTravelDestination.models';

@Component({
  selector: 'app-destination-list',
  templateUrl: './destination-list.component.html',
  styleUrls: ['./destination-list.component.css']
})
export class DestinationListComponent implements OnInit {

  @Output() onItemAdded: EventEmitter<TravelDestination>;
  updates: string [];
  all; 

  // destinos: TravelDestination [];

  // store: almacen de redux
  // Store<AppState> : Estado de la aplicacion
  constructor( public destinationApiClient: DestinationApiClient, private store: Store<AppState> ) {
    // iniicializar array vacio
    //this.destinos = [];

    // onItemAdded = creacion de una instancia de EventEmmiter (un nuevo evento custom)
    this.onItemAdded = new EventEmitter();
    this.updates = [];
    /*this.destinationApiClient.subcribeOnChange((d: TravelDestination) => {
      if(d != null){
        this.updates.push('se ha elegido a ' + d.nm);
      }
    })*/
      // redux---------------------------------------------
    // subcribe sobre un observable de redux
    // Interesan las actualizaciones sobre favorito
    this.store.select(state => state.destinos.favorito)
      .subscribe(data => {
        const d = data;
        //cambios que hay (presentan) sobre favorito

        if (d != null){
          this.updates.push("Se ha elegido a " + d.nm);
        }
      });
      store.select(state => state.destinos.items).subscribe(items => this.all = items);
      // redux fin ------------------------------------------
  }

  ngOnInit(): void {
    
    /*this.store.select(state => state.destinos)
    .subscribe(data => {
      let d = data.favorito;
      if (d != null) {
        this.updates.push("Se eligió: " + d.nm);
      }
    });*/
  }

  // push() añade uno o más elementos al final de un array y
  // devuelve la nueva longitud del array.
  // d = argumento - atributo
  agregado(d: TravelDestination){
    //this.destinos.push(d);
    this.destinationApiClient.add(d);
    this.onItemAdded.emit(d);

    // Disparando Acciones - manejado por el reducer de NuevoDestinoAction
    //this.store.dispatch(new NuevoDestinoAction(d));
  }

  elegido(e: TravelDestination) {

    // Desmarcar  todos los demás elementos en el array de elegidos
    // this.destinationApiClient.getAll().forEach(x => x.setSelected(false));
    this.destinationApiClient.elegir(e);
    // seleccionar como seleccionado
    // e.setSelected(true);

    // Disparando Acciones - Redux
    //this.store.dispatch(new ElegidoFavoritoAction(e));
  }
getAll(){}

}
