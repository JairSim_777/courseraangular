import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DestinationApiClient } from 'src/app/models/destinationApiClient.models';
import { TravelDestination } from 'src/app/models/travelDestination.models';

@Component({
  selector: 'app-destination-detail',
  templateUrl: './destination-detail.component.html',
  styleUrls: ['./destination-detail.component.css']
})
export class DestinationDetailComponent implements OnInit {
  destino: TravelDestination;

  constructor(private route: ActivatedRoute, private destinosApiClient: DestinationApiClient) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.destino = null;
    //this.destinosApiClient.getById(id);
  }

}