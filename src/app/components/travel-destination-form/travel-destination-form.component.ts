import { Component, EventEmitter, OnInit, Output } from '@angular/core';

import { TravelDestination } from '../../models/travelDestination.models';
import { map, filter, debounceTime, distinctUntilChanged, switchMap} from 'rxjs/operators';
import { FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { ajax, AjaxResponse } from 'rxjs/ajax';


@Component({
  selector: 'app-travel-destination-form',
  templateUrl: './travel-destination-form.component.html',
  styleUrls: ['./travel-destination-form.component.css']
})
export class TravelDestinationFormComponent implements OnInit {
  // evento tipo output
  @Output() onItemAdded: EventEmitter<TravelDestination>;

  fg: FormGroup;
  minLongitud = 5;
  searchResults: string[];

  constructor( fb: FormBuilder) {

    this.onItemAdded = new EventEmitter();
    // inicializacion de fg. fb (FormBuilder) utilidad para definir-construir el formulario
    //group: Objeto de inicialización que define la esctrutura del formulario

    // formControl para vincular a los tags html
    this.fg = fb.group({

      name: ['', Validators.compose([
        Validators.required,
        this.nombreValidator,
        this.nombreValidatorParametrizable(this.minLongitud)
      ])],
      url: ['']
    });

    //observador de tipeo
    this.fg.valueChanges.subscribe((form: any) => {
      console.log('Cambios en el formulario: ' + form);
    });
  }

  ngOnInit(){
    const elemNombre = <HTMLInputElement>document.getElementById('name');
    /*fromEvent observable de eventos de entrada - En este caso un string
      de eventos de teclado */
    fromEvent(elemNombre, 'input')
      .pipe(
        map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
        filter(text => text.length > 2),
        debounceTime(200),
        distinctUntilChanged(),
        switchMap(() => ajax('/assets/datos.json'))
      ).subscribe(ajaxResponse => {
        console.log(ajaxResponse);
        console.log(ajaxResponse.response);
        this.searchResults = ajaxResponse.response


        //filtramos client side solo para simplificar el ejemplo
        .filter(function(x){
          return x.toLowerCase().includes(elemNombre.value.toLowerCase());
      });
    });
  }

  // submit del formulario
  guardar(name: string, url: string): boolean {
    const d = new TravelDestination(name, url);
    // adiccionar evento
    this.onItemAdded.emit(d);
    return false;
  }

  // validador personalizado - tipo de dato de retorno - compuesto
  nombreValidator(control: FormControl): {
    // 'required' => [s: string] -- string = key  (formato sintaxis ts)
    // 'required': true
    [s: string]: boolean }{
    // l = longitud
    const l = control.value.toString().trim().length;
    if(l > 0 && l < 5 ){
      return { invalidNombret: true};
      // return { 'invalid Nombret': true};
    }
    return null;
  }

  // Validador Parametrizable

  nombreValidatorParametrizable(minLong: number): ValidatorFn {
    return (control: FormControl): {[s: string]: boolean } | null => {
      const l =control.value.toString().trim().length;
      if (l > 0 && l < minLong ){
        return { minLongNombre: true};
      }
    }
    return null;
  }
}


